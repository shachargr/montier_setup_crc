#!/bin/sh
#
#
# install from https://github.com/operator-framework/operator-sdk/blob/master/doc/user-guide.md
# Traping errors
trap 'on_error $LINENO' ERR
set -o errtrace

# get the user (in case we run in sudo
_user="$USER"

## get UID 
uid=$(id -u)
 
## Check for it
[ $uid -eq 0 ] && { echo "Please do not run this script as root or sudo."; exit 1; }
 
# get the script folder name
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" 
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
GITDIR=${DIR}
MINIKUBE_INSTALLATION="unknown"

#############
# Functions #
#############
function main() {
    log_message "INFO" "Script Start. ask for $_user password enable sudo:"
    sudo echo ""

    check_and_load_properties_files

    parse_parameters "$@"

    check_git_prerequisite

    install_mercurial

    install_golang

    reinstall_docker

    install_operator_sdk
    


    if [[ $MINIKUBE_INSTALLATION = "yes" ]]; then
            install_minikube
            install_kubectl
        else
            install_crc
			KUBE_CLI="oc" 
    fi

    add_operator_project

    log_message "INFO" "Script Completed! To debog locally:"
    #export OPERATOR_NAME=memcached-operator
    log_message "INFO" "# export OPERATOR_NAME=memcached-operator"
    log_message "INFO" "# /usr/local/bin/operator-sdk run --local --namespace=default"
}

function parse_parameters() {
    while [[ $# -gt 0 ]]; do
        case "$1" in
        -h | --help)
            show_help
            exit 0
            ;;
        -m | --minikube)
            MINIKUBE_INSTALLATION="yes"
            shift
            ;;
        *)
            log_message "ERROR" "Unknown parameter $1"
            show_help
            exit 250
            ;;
        esac
    done
}

function show_help() {
    echo ""
    log_message "INFO" "Usage: $0 -m|--minikube [-d|--delete]"
    log_message "INFO" " "
    log_message "INFO" "       -m|--minikube    : Install and run minikube. By default the script won't run minikube"
    log_message "INFO" "       -h|--help   : Display help"
    log_message "INFO" " "
    log_message "INFO" "For Example: "
    log_message "INFO" "       $0 -m"
}

function log_message() {
    local LOG_LEVEL="$1"
    local LOG_MESSAGE="$2"
    echo "$(date +%F_%H-%M-%S) $LOG_LEVEL $LOG_MESSAGE"
}

function on_error() {
    local LINE_NO="$1"
    log_message "ERROR" "Error occurred on line $LINE_NO. Aborting."
    exit 250
}

function pause() {
    read -p "$*"
}

function check_and_load_properties_files() {
    file="./build.properties"

    if [ -f "$file" ]; then
        log_message "INFO" "$file found."
        source $file
    else
        log_message "INFO" "$file not found!"
        exit 251
    fi

}

function check_git_prerequisite() {
    log_message "INFO" "check_git_prerequisite - check if git installed"
    local IS_GIT_EXISTS=0
    git --version >/dev/null 2>&1 && IS_GIT_EXISTS=1

    if [[ ${IS_GIT_EXISTS} -eq 1 ]]; then
        log_message "INFO" "check_git_prerequisite - Git Installed! "
    else
        log_message "Error" "check_git_prerequisite -Git Not installed! please install it manually"
        exit 252
    fi
}

#echo "install bazaar" - problematic and might not be needed
#su -c 'rpm -Uvh ${INSTALL_EPEL_BAZZAR}'
#yum -y install bzr

################ mercurial - start
function install_mercurial() {
    local IS_MERCURIAL_EXISTS=0

    log_message "INFO" "install_mercurial - Check if mercurial cli installed (rpm -q mercurial)"
    rpm -q mercurial >/dev/null 2>&1 && IS_MERCURIAL_EXISTS=1

    if [[ ${IS_MERCURIAL_EXISTS} -eq 0 ]]; then

        log_message "INFO" "install_mercurial - Start installation of Mercurial: " ${INSTALL_MERCURIAL}
        curl -LO ${INSTALL_MERCURIAL}
        sudo rpm -Uvh ${MERCURIAL_RPM}

        rm ${MERCURIAL_RPM}
    fi 

    IS_MERCURIAL_EXISTS=0

    log_message "INFO" "install_mercurial - Check if mercurial cli installed (hg)"
    hg --version >/dev/null 2>&1 && IS_MERCURIAL_EXISTS=1

    if [[ ${IS_MERCURIAL_EXISTS} -eq 1 ]]; then
        log_message "INFO" "install_mercurial - HG mercurial Installed! "
    else
            log_message "Error" "install_mercurial - HG mercurial Not installed! please install it manually"
            exit 252
    fi
    
}

################ mercurial - end
################ golang - start
function install_golang() {

    local IS_GO_EXISTS=0
    source $HOME/.bash_profile
    go version >/dev/null 2>&1 && IS_GO_EXISTS=1

    if [[ ${IS_GO_EXISTS} -eq 1 ]]; then
        log_message "INFO" "Golang Installed! No need to reinstall."
    else
        log_message "INFO" "Start installation of golang: " ${GOLANG_URL}
        curl -LO ${GOLANG_URL}
        sudo tar -C /usr/local -xzf go$GO_VERSION.linux-amd64.tar.gz
        echo "export PATH=$PATH:/usr/local/go/bin" >>$HOME/.bash_profile
        source $HOME/.bash_profile
        rm go$GO_VERSION.linux-amd64.tar.gz
    fi

    log_message "INFO" "checking if golang cli installed"
    go version
}

################ goalang - end

################ docker - start
function reinstall_docker() {
    	log_message "INFO" "Remove old docker versions"
    	local IS_DOCKER_EXISTS=0

    	sudo rpm -qa | grep -qvws remove docker || sudo yum -y -q remove docker && IS_DOCKER_EXISTS=$(($IS_DOCKER_EXISTS + 1))
    	log_message "INFO" "$IS_DOCKER_EXISTS - Remove old docker - docker"

    	sudo rpm -qa | grep -qvws remove docker-client || sudo yum remove -y -q docker-client && IS_DOCKER_EXISTS=$(($IS_DOCKER_EXISTS + 1))
	log_message "INFO" "$IS_DOCKER_EXISTS - Remove old docker - docker-client"

    	sudo rpm -qa | grep -qvws remove docker-client-latest || sudo yum remove -y -q docker-client-latest && IS_DOCKER_EXISTS=$(($IS_DOCKER_EXISTS + 1))
   	log_message "INFO" "$IS_DOCKER_EXISTS - Remove old docker - docker-client-latest"

    	sudo rpm -qa | grep -qvws remove docker-common || sudo yum remove -y -q docker-common && IS_DOCKER_EXISTS=$(($IS_DOCKER_EXISTS + 1))
    	log_message "INFO" "$IS_DOCKER_EXISTS - Remove old docker - docker-common"

    	sudo rpm -qa | grep -qvws remove docker-latest || sudo yum remove -y -q docker-latest && IS_DOCKER_EXISTS=$(($IS_DOCKER_EXISTS + 1))
    	log_message "INFO" "$IS_DOCKER_EXISTS - Remove old docker - docker-latest"

    	sudo rpm -qa | grep -qvws remove docker-latest-logrotate || sudo yum remove -y -q  docker-latest-logrotate && IS_DOCKER_EXISTS=$(($IS_DOCKER_EXISTS + 1))
    	log_message "INFO" "$IS_DOCKER_EXISTS - Remove old docker - docker-latest-logrotate"

    	sudo rpm -qa | grep -qvws remove docker-logrotate || sudo yum remove -y -q  docker-logrotate && IS_DOCKER_EXISTS=$(($IS_DOCKER_EXISTS + 1))
    	log_message "INFO" "$IS_DOCKER_EXISTS - Remove old docker - docker-logrotate"

    	sudo rpm -qa | grep -qvws remove  docker-engine || sudo yum remove -y -q  docker-engine && IS_DOCKER_EXISTS=$(($IS_DOCKER_EXISTS + 1))
    	log_message "INFO" "$IS_DOCKER_EXISTS - Remove old docker -  docker-engine"

    #yum -y remove docker \
    #    docker-client \
    #    docker-client-latest \
    #    docker-common \
    #    docker-latest \
    #    docker-latest-logrotate \
    #    docker-logrotate \
    #    docker-engine >/dev/null 2>&1 && IS_DOCKER_EXISTS=1

    if [[ ${IS_DOCKER_EXISTS} -eq 8 ]]; then
        log_message "INFO" "Docker older versions removed!"
    else
        log_message "EROR" "Docker older versions *cannot be* reomved!"
	exit 253
    fi

    local IS_DOCKER_ECO_SYSTEM_EXISTS=0
    log_message "INFO" "Install required packages if needed"
    sudo yum install -y -q yum-utils \
        device-mapper-persistent-data \
        lvm2 >/dev/null 2>&1 && IS_DOCKER_ECO_SYSTEM_EXISTS=1

    if [[ ${IS_DOCKER_ECO_SYSTEM_EXISTS} -eq 1 ]]; then
        log_message "INFO" "Install required packages check OK! "
    else
        log_message "ERROR" "Install required packages failed!"
	exit 250
    fi

    log_message "INFO" "set stable repo for docker"
    sudo yum-config-manager \
        --add-repo \
        ${DOCKER_REPO}

    log_message "INFO" "Install docker"
    sudo yum-config-manager --disable docker-ce-nightly
    sudo yum-config-manager --disable docker-ce-test
    sudo yum -y -q install docker-ce docker-ce-cli containerd.io

    sudo usermod -aG docker ${USER}


    log_message "INFO" "restart docker service"
    sudo systemctl start docker
    sudo systemctl enable docker
    log_message "INFO" "start docker hello-worls as a sudo (cannot do it else)"
    sudo setfacl --modify user:${whoami}:rw /var/run/docker.sock
    docker run hello-world
}

################ docker - end

################ kubectl - start
function install_kubectl() {
    log_message "INFO" "Add k8s repo"

    cat <<EOF | sudo tee -a /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=${KUBECTL_REPO}
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=${KUBECTL_REPO_GPG} 
EOF

    log_message "INFO" "install kubectl"
    sudo yum -y install -y kubectl
}
################ kubectl - end

#################### Delve - start
### to decide if add it to environment
#################### Delve - start

#################### sdk - start
function install_operator_sdk() {
    local IS_OPERATOR_EXISTS=0
    /usr/local/bin/operator-sdk version >/dev/null 2>&1 && IS_OPERATOR_EXISTS=1

    if [[ ${IS_OPERATOR_EXISTS} -eq 1 ]]; then 
        log_message "INFO" "install_operator_sdk - No Need to install SDK"
    else
        log_message "INFO" "install_operator_sdk - Start installation of SDK: " ${OPERATOR_SDK_LINK}
        curl -LO ${OPERATOR_SDK_LINK}

        log_message "INFO" "install_operator_sdk - Download installation of SDK ASC file: " ${OPERATOR_SDK_LINK}
        curl -LO ${OPERATOR_SDK_LINK_ASC}

        log_message "INFO" "install_operator_sdk - Verify installation download of SDK: "

        local IS_GPG_OK=0
        ERROR=$(gpg --verify operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu.asc 2>&1) && IS_GPG_OK=1

        if [[ ${IS_GPG_OK} -eq 1 ]]; then
            log_message "INFO" "install_operator_sdk - Verification succeeded!! "
        else
            log_message "Info" "install_operator_sdk -error of gpg: $ERROR"
            KEY=$(echo $ERROR | awk -F'using RSA key ID' '{print $2}')
            KEY=$(echo $KEY | awk '{print $1}')
            log_message "INFO" "install_operator_sdk -key" $KEY

            log_message "INFO" "install_operator_sdk -recieve key and run gpg again with key"
            gpg --recv-key "$KEY"
            gpg --verify operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu.asc
        fi

        log_message "INFO" "install_operator_sdk - Install operator !"
        chmod +x operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu && mkdir -p /usr/local/bin/ && sudo cp operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu /usr/local/bin/operator-sdk && rm operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu
        rm operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu.asc
    fi

    log_message "INFO" "install_operator_sdk -Verify operator sdk installation... $(whoami)"
    /usr/local/bin/operator-sdk version
}
#################### sdk - end

#################### minikube - start
function install_minikube() {
    log_message "INFO" "Minikube Installation.."

    if [[ $MINIKUBE_DRIVER = "docker" ]]; then
        log_message "INFO" "Minikube install docker prerequistes.."
        # to disable error:" dial unix /var/run/docker.sock: connect: permission denied."  on minikube and other scenrios
        sudo setfacl -m user:${_user}:rw /var/run/docker.sock
    fi

    if [[ $MINIKUBE_DRIVER = "kvm2" ]]; then
        log_message "INFO" "Minikube install kvm2 driver prerequistes.."
        log_message "INFO" "Minikube - check and configure livrtd"
        sudo yum -y install qemu-kvm libvirt libvirt-python libguestfs-tools virt-install
        log_message "INFO" "Minikube - check and configure livrtd"
        systemctl status libvirtd
        log_message "INFO" "Enabling libvirtd"
        sudo systemctl enable --now libvirtd
        log_message "INFO" "adding ${_user} to libvirtd"
        sudo usermod -a -G libvirt ${_user}
        #log_message "INFO" "Creating new group livrt"
        #sudo newgrp libvirt
        # https://computingforgeeks.com/how-to-install-minikube-on-centos-linux-with-kvm/
        # and https://www.cyberciti.biz/faq/how-to-install-kvm-on-centos-7-rhel-7-headless-server/
        local LIBVIRT_FILE=/etc/libvirt/libvirtd.conf
        log_message "INFO" "Replace $LIBVIRT_FILE values to allow virtualization for minikube and restart it"
        sudo sed -i '/unix_sock_group/s/^#//g' $LIBVIRT_FILE
        sudo sed -i '/unix_sock_ro_perms/s/^#//g' $LIBVIRT_FILE
        #unix_sock_ro_perms = "0777" unix_sock_ro_perms="0770"
        sudo systemctl restart libvirtd.service
    fi

    local IS_MINIKUBE_EXISTS=0
    /usr/local/bin/minikube version >/dev/null 2>&1 && IS_MINIKUBE_EXISTS=1
    if [[ ${IS_MINIKUBE_EXISTS} -eq 1 ]]; then 
        log_message "INFO" " Minikube installed no need to install"
        minikube stop
        minikube delete
    else
        log_message "INFO" " Minikube: install wget - check wget"
        sudo yum -y install wget
        wget $MINIKUBE_URL
        chmod +x minikube-linux-amd64
        sudo mv minikube-linux-amd64 /usr/local/bin/minikube
    fi

    log_message "INFO" "  Verify minikube installation...$(whoami)"
    /usr/local/bin/minikube version
    minikube start --driver $MINIKUBE_DRIVER
    eval $(minikube docker-env)
}


### ------------------------------------- ###
###      install_crc
### ------------------------------------- ###
function install_crc() {  
	# CHECK We can install on this machine CRC	
	log_message "INFO" "install_crc - Check machine meeting requirments."
	check_prereq

	# CHECK If CRC installed already
	log_message "INFO" "install_crc - Check if CRC was installed before."
	IS_CRC_EXISTS=$(is_crc_installed_before)
	IS_CRC_EXISTS=$(tr -dc '[[:print:]]' <<< "$IS_CRC_EXISTS")
	log_message "INFO" "install_crc - Function is_crc_installed return ${IS_CRC_EXISTS} hanan."
	echo HANAN

	# DEPLOY CRC - if CRC is not exists ( MISSING ) than we should deploy it 
	if [[ "$IS_CRC_EXISTS" == "MISSING" ]]; then
		log_message "INFO" "install_crc - CRC was NOT installed and it will be deployed now."
		deploy_crc
		exit_if_crc_not_installed
	fi

	# at this point we assume CRC is installed
	log_message "INFO" "install_crc - Check if CRC installation is valid."
	CRC_VERSION_VALID=$(is_valide_crc_installtion)
	
	if [[ "${CRC_VERSION_VALID}" == "INVALID" ]]; then
		log_message "ERROR" "install_crc - Failed to validate CRC installation. crc version command failed. Try to run with param full cleanup"
		perform_full_clean
		deploy_crc
		exit_if_crc_not_installed
		exit_if_crc_installation_not_valid
	fi
	log_message "INFO" "install_crc - CRC installation is valid ${IS_CRC_VALID}"

	# we will check the crc status (up and running, shutdown, unknowm)
	#is_crc_running
	IS_CRC_STATUS_VALID=$(is_crc_status_valid)
	if [[ ${IS_CRC_STATUS_VALID} == 'VALID_STATUS' ]]; then
		launch_crc
	else 
		# if src is installed and but we are unable to get the status we should reinstall it start it or even reinstall it.
			log_message "INFO" "install_crc - CRC is at unknow state ${IS_CRC_UP}. reinstall required"			
			reinstall_crc
			start_crc
			wait_crc_until_running			
	fi

	log_message "INFO" "install_crc - CRC is running"
	connectToCrc

}

### ------------------------------------- ###
###      reinstall_crc
### ------------------------------------- ###
function reinstall_crc() {
	log_message "INFO" "reinstall_crc - start full cleaning"
	perform_full_clean
	log_message "INFO" "reinstall_crc - start deploy_crc"
	deploy_crc
	log_message "INFO" "reinstall_crc - validate crc install"
	exit_if_crc_not_installed
	exit_if_crc_installation_not_valid
	log_message "INFO" "reinstall_crc - setup crc"

}

### ------------------------------------- ###
###      start_crc
### ------------------------------------- ###
function start_crc() {
	log_message "INFO" "start_crc - About to start CRC - 20 min"
	crc_setup

	log_message "INFO" "start_crc - completed. about to start CRC"
	local 	IS_CRC_STARTED=0
	crc start -p /tmp/pull-secret.txt ${CRC_DEBUG} > /tmp/install_crc.log 2>&1 && IS_CRC_STARTED=1
	if [[ ${IS_CRC_STARTED} -eq 1 ]]; then 
		log_message "INFO" "start_crc - CRC starting command return with RC=${IS_CRC_STARTED}"
		IS_CRC_STARTED="STARTED"
	else
		log_message "ERROR" "start_crc - CRC starting command failed - see logs /tmp/install_crc.log"
		cat /tmp/install_crc.log
		IS_CRC_STARTED="NOT_STARTED"
	fi
}

### ------------------------------------- ###
###      launch_crc
### ------------------------------------- ###
function launch_crc() {
	log_message "INFO" "launch_crc - Obtain crc status (up and running, shutdown, unknown)"
	local IS_CRC_UP=1
	crc status >/dev/null 2>&1 && IS_CRC_UP=1
	if [[ ${IS_CRC_UP} -eq 1 ]]; then

		# CRC status return value
       		IS_OpenShift_Running=$(crc status --log-level debug | grep 'OpenShift' | awk '{print $2}')
		
	        CRC_Domain_Exists=$(sudo virsh list --all | grep -q crc | awk '{print $2}')
	        CRC_Domain_Status=$(sudo virsh list --all | grep -q crc | awk '{print $3}')
	        log_message "INFO"  "launch_crc - CRC Virt Domain_Exists:${CRC_Domain_Exists}. CRC Virt Domain Status:${CRC_Domain_Status}. CRC status:${IS_OpenShift_Running}"
		if [[ 	"${CRC_Domain_Exists}" == "" ]]; then
			start_crc
		fi
       		IS_OpenShift_Running=$(crc status --log-level debug | grep 'OpenShift' | awk '{print $2}')
		
	        CRC_Domain_Exists=$(sudo virsh list --all | grep -q crc | awk '{print $2}')
	        CRC_Domain_Status=$(sudo virsh list --all | grep -q crc | awk '{print $3}')
	        log_message "INFO"  "launch_crc - CRC Virt Domain_Exists:${CRC_Domain_Exists}. CRC Virt Domain Status:${CRC_Domain_Status}. CRC status:${IS_OpenShift_Running}"
		if [[ 	"${IS_OpenShift_Running}" == "" ]]; then
			# handle ERRO error: stat /home/montieruser/.crc/machines/crc/kubeconfig: no such file or directory
			log_message "INFO" "launch_crc - Probably -found error - ERRO error: stat /home/montieruser/.crc/machines/crc/kubeconfig: no such file or directory "
			perform_deep_clean
    #    # this is  an hack based on this: http://www.mastertheboss.com/soa-cloud/openshift/getting-started-with-code-ready-containers
        		#crc stop 
        		#crc delete -f
			#TODO check if domain CRC exist before run the virsh command
			#CRC_Domain_Exists=$(sudo virsh list --all | grep -q crc | awk '{print $2}')
			#if [[ 	"${CRC_Domain_Exists}" == "crc" ]]; then 
        		#	sudo virsh undefine crc --remove-all-storage
        		#	sudo virsh net-destroy crc
			#fi
		        sudo rm -f /etc/NetworkManager/conf.d/crc-nm-dnsmasq.conf /etc/NetworkManager/dnsmasq.d/crc.conf
		        sudo systemctl reload NetworkManager
		        crc setup
		        crc start -p /tmp/pull-secret.txt ${CRC_DEBUG} > /tmp/install_crc.log 2>&1
			wait_crc_until_running		
			
		fi
       		IS_OpenShift_Running=$(crc status --log-level debug | grep 'OpenShift' | awk '{print $2}')
	        CRC_Domain_Exists=$(sudo virsh list --all | grep -q crc | awk '{print $2}')
	        CRC_Domain_Status=$(sudo virsh list --all | grep -q crc | awk '{print $3}')
	        log_message "INFO"  "launch_crc - CRC Virt Domain_Exists:${CRC_Domain_Exists}. CRC Virt Domain Status:${CRC_Domain_Status}. CRC status:${IS_OpenShift_Running}"


		# if src is installed and not up we should start it or even reinstall it.
		if [[ ${IS_OpenShift_Running} == "Stopped" ]]; then		
			log_message "INFO" "install_crc - CRC is stopped ${IS_CRC_UP}"
			start_crc
			wait_crc_until_running
		fi
		#if [[ ${IS_OpenShift_Running} == "" ]]; then		
		#	log_message "INFO" "install_crc - CRC is not started ${IS_CRC_UP} we will try to start it"
		#	start_crc
		#	start_crc
		#fi

	fi
}
### ------------------------------------- ###
###      exit_if_crc_installation_not_valid
### ------------------------------------- ###
function exit_if_crc_installation_not_valid() {
	log_message "INFO" "exit_if_crc_installation_not_valid - validate CRC installation"
	CRC_VERSION_VALID=$(is_valide_crc_installtion)
	log_message "INFO" "exit_if_crc_installation_not_valid - function return ${IS_CRC_VALID}"
	if [[ "${CRC_VERSION_VALID}" == "INVALID" ]]; then
		log_message "FATAL" "exit_if_crc_installation_not_valid - Failed to validate CRC installation. crc version command failed again..."
		return 11 # FATAL - Installation failed. DO Not try to retry. manual steps required . try to dig it from logs
	fi
}
### ------------------------------------- ###
###      exit_if_crc_not_installed
### ------------------------------------- ###
function exit_if_crc_not_installed() {
	log_message "INFO" "exit_if_crc_not_installed - Checks if CRC installation is valid."
	IS_CRC_EXISTS=$(is_crc_installed_before)
	log_message "INFO" "After exit_if_crc_not_installed - function return ${IS_CRC_EXISTS}"
	if [[ "${IS_CRC_EXISTS}" == "EXISTS" ]]; then
		log_message "FATAL" "exit_if_crc_not_installed - Failed to find CRC binaries at /usr/local/bin/crc"
		return 17 # FATAL - Installation failed. DO Not try to retry. manual steps required . try to dig it from logs
	fi
}
### ------------------------------------- ###
###      check_prereq
### ------------------------------------- ###
function check_prereq() {
	log_message "INFO" " !!! IF this line is failint than you should  read the doc - especially this line - For vmware workstation you should make sure to have on the VM machine settings in the 'Processors' section you should have few checkboxes for 'Virtualization engine' and make sure the 'Virtualize Intel VT-x/EPT' checkbox is enabled and all others are disabled."
	sleep 3
	lsmod | grep -i kvm
	sleep 3
   	brctl show
	# TODO check memory
	# TODO check space
}


### ------------------------------------- ###
###      clean_crc_folder
### ------------------------------------- ###
function clean_crc_folder() {
        log_message "INFO" "Perform folders cleanup.."
        rm -fR ~/.crc/
        rm -fR ~/.kube/
        rm -fR /tmp/crc*/
        sudo rm -Rf /usr/local/bin/crc
        sudo rm -f /etc/NetworkManager/conf.d/crc-nm-dnsmasq.conf
        sudo rm -f /etc/NetworkManager/dnsmasq.d/crc.conf
        sudo rm -f /etc/libvirt/qemu/networks/autostart/crc.xml
        sudo rm -f /etc/libvirt/qemu/networks/crc.xml
        sudo rm -f /etc/libvirt/qemu/crc.xml
        sudo rm -f /var/lib/libvirt/dnsmasq/crc.conf
        sudo rm -f /var/lib/libvirt/dnsmasq/crc.hostsfile
        sudo rm -f /var/lib/libvirt/dnsmasq/crc.addnhosts
        sudo rm -f /var/lib/libvirt/dnsmasq/crc.status
        sudo rm -f /var/lib/libvirt/dnsmasq/crc.macs
        sudo rm -fR /var/lib/libvirt/qemu/channel/target/domain-1-crc
        sudo rm -fR /var/lib/libvirt/qemu/domain-1-crc
        sudo rm -f /var/log/libvirt/qemu/crc.log
	log_message "INFO" "Folders cleanup ended"
}

### ------------------------------------- ###
###      deploy_crc
### ------------------------------------- ###
function deploy_crc() {
        
	#sudo yum -y install qemu-kvm libvirt libvirt-python libguestfs-tools virt-install NetworkManager bridge-utils libvirt-daemon-driver-qemu virt-manager
        #sudo yum -y install qemu-kvm libvirt virt-install bridge-utils NetworkManager libvirt-daemon-driver-qemu 
	clean_crc_folder

	log_message "INFO" "enable libvrt.."
	sudo rpm -qa | grep -qw qemu-kvm || yum install qemu-kvm
	sudo rpm -qa | grep -qw libvirt || yum install libvirt
	sudo rpm -qa | grep -qw libvirt-python || yum install libvirt-python
	sudo rpm -qa | grep -qw libguestfs-tools || yum install libguestfs-tools
	sudo rpm -qa | grep -qw virt-install || yum install virt-install
	sudo rpm -qa | grep -qw NetworkManager || yum install NetworkManager
	sudo rpm -qa | grep -qw bridge-utils || yum install bridge-utils
	sudo rpm -qa | grep -qw libvirt-daemon-driver-qemu || yum install libvirt-daemon-driver-qemu
	sudo rpm -qa | grep -qw virt-manager || yum install virt-manager
        
	sudo systemctl enable --now libvirtd 
        sudo systemctl restart libvirtd
        curr_dir="$(pwd)"
        log_message "INFO" "Download 2G of CRC binary and COPY CRC to PATH and delete the extracted library.. $curr_dir"
        #For debug ony !! you can cache this file if you set comment on this line -  wget $CRC_URL --output-file /tmp/crc-linux-amd64.tar.xz
	# TODO check we have space here
        cd  /tmp
        if [ ! -f /tmp/crc-linux-amd64.tar.xz ]; then	
            wget  --progress=bar:force -c -r -O /tmp/crc-linux-amd64.tar.xz $CRC_URL
        fi

        log_message "INFO" "Extract CRC binary and delete it.."
        tar xvf /tmp/crc-linux-amd64.tar.xz
        
	if [[ "$DEV_MODE" != "true" ]]; then
	        rm -rf /tmp/crc-linux-amd64.tar.xz
	fi
        cd /tmp/crc*/
        sudo cp crc /usr/local/bin
        cd ..
        rm -Rf /tmp/crc*/
}
### ------------------------------------- ###
###      is_crc_running
### ------------------------------------- ###
function is_crc_status_valid() {
	#log_message "INFO" "is_crc_running - Obtain crc status (up and running, shutdown, unknown)"
	local IS_CRC_UP=1
	crc status >/tmp/crc_status.txt 2>&1 && IS_CRC_UP=1
	if [[ ${IS_CRC_UP} -eq 1 ]]; then
		local  retval='VALID_STATUS'
		echo "$retval"
	else
		#log_message "FATAL" "is_crc_running - crc command failed." 
		#cat crc_status.txt
		local  retval='INVALID_STATUS'
		echo "$retval"
	fi
}
### ------------------------------------- ###
###      is_crc_installed_before
### ------------------------------------- ###
function is_crc_installed_before() {
	if [ -f /usr/local/bin/crc ]; then
		    local  retval='EXISTS'
		    echo "$retval"
		    ##ls  -la /usr/local/bin/crc*
	else
		    local  retval='MISSING'
		    echo "$retval"
	fi
}
### ------------------------------------- ###
###      is_valide_crc_installtion
### ------------------------------------- ###
function is_valide_crc_installtion() {
    local isValid=1
    crc version >/dev/null 2>&1 && isValid=1
    if [[ ${isValid} -eq 1 ]]; then 

	message=$(crc version)  
	local  retval='VALID'
	echo "$retval"
    else
	local  retval='INVALID'
	echo "$retval"
    fi
}
### ------------------------------------- ###
###      stop_crc
### ------------------------------------- ###
function stop_crc() {
	log_message "INFO" "stop_crc - Performing stop CRC"
        crc stop 
	wait_crc_until_stopped
	
}


### ------------------------------------- ###
###      wait_crc_until_stopped
### ------------------------------------- ###
function wait_crc_until_stopped() {
	log_message "INFO" "wait_crc_until_stopped - for 120 sec"
  	index=1
	local IS_OpenShift_Running_1a=$(crc status --log-level debug | grep 'OpenShift' | awk '{print $2}')
	while [[ $index -le 40 && $IS_OpenShift_Running_1a != "Stopped" ]]; do
		sleep 3s
     	        IS_OpenShift_Running_1a=$(crc status --log-level debug | grep 'OpenShift' | awk '{print $2}')
		log_message "INFO" "wait_crc_until_stopped - Waiting for openShift to stop - status ${IS_OpenShift_Running_1a} - wait number:$index"
		(( index++ ))
	done 
	if [[ $IS_OpenShift_Running_1a == "Stopped" ]]; then
		log_message "INFO" "wait_crc_until_stopped - CRC was stopped succesfully"
		return 0
	else
		log_message "FATAL" "wait_crc_until_stopped -Unable to stop CRC after 120s"
		exit 13;
	fi
	
}

### ------------------------------------- ###
###      wait_crc_until_running
### ------------------------------------- ###
function wait_crc_until_running() {
	log_message "INFO" "wait_crc_until_running - for 400 sec"
  	index=1
	local IS_OpenShift_Running_1a=$(crc status --log-level debug | grep 'OpenShift' | awk '{print $2}')
	while [[ $index -le 40 && $IS_OpenShift_Running_1a != "Running" ]]; do
		sleep 10s
     	        IS_OpenShift_Running_1a=$(crc status --log-level debug | grep 'OpenShift' | awk '{print $2}')
		log_message "INFO" "wait_crc_until_running - Waiting for openShift to stop - status ${IS_OpenShift_Running_1a} - wait number:$index"
		(( index++ ))
		Is_Memory_Problem=$(grep -i '(crc) Failed to start:.*Cannot allocate memory.*'   /tmp/install_crc.log)
		if [[ -n ${Is_Memory_Problem} ]]; then
       			log_message "Fatal" "Stopping CRC - memory allocation issue detected. Check Operating system prereq"		
			Exit 20
		fi
	done 
	if [[ $IS_OpenShift_Running_1a == "Running" ]]; then
		log_message "INFO" "wait_crc_until_running - CRC was started succesfully"
		return 0
	else
		log_message "FATAL" "wait_crc_until_running -Unable to start CRC after 400s"
		exit 16;
	fi
	
}
### ------------------------------------- ###
###      perform_deep_clean
### ------------------------------------- ###
function perform_deep_clean {

	# CHECK If CRC installed already - if not we can delete it 
	echo log_message "INFO" "perform_deep_clean - Check if CRC was installed before."
	IS_CRC_EXISTS=$(is_crc_installed_before)
	
	echo log_message "INFO" "perform_deep_clean - function return ${IS_CRC_EXISTS}"

	# IF CRC EXIST we should see its status before we can remove it  
	if [[ ${IS_CRC_EXISTS} == "EXISTS" ]]; then
		echo log_message "INFO" "perform_deep_clean - CRC was installed and it will be stopped now if not already"
		CRC_Domain_Exists=$(sudo virsh list --all | grep -q crc | awk '{print $2}')
		if [[ 	"${CRC_Domain_Exists}" == "crc" ]]; then 
			local IS_CRC_UP=0
			crc status >/dev/null 2>&1 && IS_CRC_UP=1
			# CRC IS up and we should stop it
			if [[ ${IS_CRC_UP} -eq 0 ]]; then
        	    		log_message "INFO"  "perform_deep_clean - Stopping CRC."
				stop_crc
			else 
				wait_crc_until_stopped
			fi
			log_message "INFO"  "perform_deep_clean - At this point CRC is stopped. Tring to delete CRC."		
			crc delete -f 
			log_message "INFO"  "perform_full_clean - CRC deleted."
		fi
		CRC_Domain_Exists=$(sudo virsh list --all | grep -q crc | awk '{print $2}')
		if [[ 	"${CRC_Domain_Exists}" == "crc" ]]; then 
    			sudo virsh undefine crc --remove-all-storage
        		sudo virsh net-destroy crc
		fi
	fi

	cleanVirtualNetwork

	clean_crc_folder

	log_message "INFO" "perform_full_clean -  CRC installation is removed ${IS_CRC_VALID}"
}
### ------------------------------------- ###
###      perform_full_clean
### ------------------------------------- ###
function perform_full_clean() {

	# CHECK If CRC installed already - if not we can delete it 
	echo log_message "INFO" "perform_full_clean - Check if CRC was installed before."
	IS_CRC_EXISTS=$(is_crc_installed_before)

	echo log_message "INFO" "perform_full_clean - function return ${IS_CRC_EXISTS}"

	# IF CRC EXIST we should see its status vefore we can remove it  
	if [[ ${IS_CRC_EXISTS} == "EXISTS" ]]; then
		echo log_message "INFO" "perform_full_clean - CRC was installed and it will be stopped now if not already"

		IS_CRC_STATUS_VALID=$(is_crc_status_valid)
		if [[ ${IS_CRC_STATUS_VALID} == 'VALID_STATUS' ]]; then
			# CRC IS up and we should stop it
			#TODO - We should check the status is stopped 
            		log_message "INFO"  "perform_full_clean - Stopping CRC."
			stop_crc
		else 
			wait_crc_until_stopped
		fi
		log_message "INFO"  "perform_full_clean - At this point CRC is stopped. Tring to delete CRC."		
		crc delete -f 
		log_message "INFO"  "perform_full_clean - CRC deleted."
	fi

	cleanVirtualNetwork

	log_message "INFO" "perform_full_clean -  CRC installation is removed ${IS_CRC_VALID}"
}
### ------------------------------------- ###
###      crc_setup
### ------------------------------------- ###
function crc_setup() {
        log_message "INFO" "crc_setup -Running CRC setup."
        crc setup

        my_ip1=$(cat /etc/NetworkManager/dnsmasq.d/crc.conf | grep 'server=/crc.testing/' | awk -F '/' '{print $3}')

        log_message "INFO" "Extract CRC IP after setup: $my_ip1"
        my_host="api.crc.testing"
        a=$((sudo grep -q "$my_host" /etc/hosts) && (sudo sed -i "/$my_host/ s/.*/$my_ip1\t$my_host/g" /etc/hosts) || (echo "$my_ip1  $my_host" | sudo tee -a /etc/hosts))

        my_host="console-openshift-console.apps-crc.testing"
        b=$((sudo grep -q "$my_host" /etc/hosts) && (sudo sed -i "/$my_host/ s/.*/$my_ip1\t$my_host/g" /etc/hosts) || (echo "$my_ip1  $my_host" | sudo tee -a /etc/hosts))

        my_host="oauth-openshift.apps-crc.testing"
        c=$((sudo grep -q "$my_host" /etc/hosts) && (sudo sed -i "/$my_host/ s/.*/$my_ip1\t$my_host/g" /etc/hosts) || (echo "$my_ip1  $my_host" | sudo tee -a /etc/hosts))
        
	d=$((sudo grep -q "$search localdomain" /etc/resolv.conf && sudo sed -i -n -e '/^search localdomain/!p' -e '$asearch redhat.com' /etc/resolv.conf))

        log_message "INFO" "read redhat image pull secret if exists .."
        if [ ! -f /tmp/pull-secret.txt ]; then
            read -p "Please paste redhat image pull secret (https://cloud.redhat.com/openshift/install/crc/installer-provisioned) and press [ENTER]:"
            echo "$REPLY" > /tmp/pull-secret.txt
        fi

        ### ovveride dns issues https://medium.com/@trlogic/how-to-setup-local-openshift-4-cluster-with-red-hat-codeready-containers-6c5aefba72ad
        # extract IP that was set in "crc setup" in /etc/NetworkManager/dnsmasq.d/crc.conf
        log_message "INFO" "crc_setup -Completed."

}
### ------------------------------------- ###
###      cleanVirtualNetwork
### ------------------------------------- ###
function cleanVirtualNetwork() {
        # run command "sudo virsh list --all " and if there is an crc domain in kvm you must remove it with command  "sudo virsh undefine crc" (SUDO IS MUST !!)
        log_message "INFO" "cleanVirtualNetwork - Remove CRC and check if CRC domain exists in KVM"
        IS_OpenShift_Running=$(crc status --log-level debug | grep 'OpenShift' | awk '{print $2}')
        CRC_Domain_Exists=$(sudo virsh list --all | grep crc | awk '{print $2}')
        CRC_Domain_Status=$(sudo virsh list --all | grep crc | awk '{print $3}')
        log_message "INFO"  "cleanVirtualNetwork - KVM Domain still exists : --CRC_Domain_Exists:${CRC_Domain_Exists}--CRC Domain Status:${CRC_Domain_Status}---IS_OpenShift_Running:${IS_OpenShift_Running}"
        if [[ "${IS_OpenShift_Running}" != "running" ]]; then
        	if [[ "${CRC_Domain_Exists}" == "crc" ]]; then
        		log_message "info" "clean existing CRC domain"
			#TODO what to do if it is in stopping mode 
        		if [[ "${CRC_Domain_Status}" == "running" ]]; then
		                sudo virsh shutdown crc
		                sleep 10s
				local index=1
				CRC_Domain_Exists=$(sudo virsh list --all | grep crc | awk '{print $2}')
				CRC_Domain_Status=$(sudo virsh list --all | grep crc | awk '{print $3}')

		                while [[  $index -le 40 && "${CRC_Domain_Status}" == "running" ]]; do
		                    	sleep 3s
					(( index++ ))
				        CRC_Domain_Exists=$(sudo virsh list --all | grep crc | awk '{print $2}')
				        CRC_Domain_Status=$(sudo virsh list --all | grep crc | awk '{print $3}')
					log_message "INFO"  "cleanVirtualNetwork - KVM Domain still exists : CRC_Domain_Exists:${CRC_Domain_Exists}--CRC Domain Status:${CRC_Domain_Status}---IS_OpenShift_Running:${IS_OpenShift_Running} for ${index} times"
		                done
		                CRC_Domain_Status=$(sudo virsh list --all | grep crc | awk '{print $3}')
				log_message "INFO"  "cleanVirtualNetwork - KVM Domain status after waiting is:: CRC_Domain_Exists:${CRC_Domain_Exists}--CRC Domain Status:${CRC_Domain_Status}---IS_OpenShift_Running:${IS_OpenShift_Running} for ${index} times"
				if [[ "${CRC_Domain_Status}" != "stopped" ]]; then
					log_message "FATAL"  "cleanVirtualNetwork - unable to remove KVM CRC Domain. use virsh command to handle this."
					exit 15
				fi
			fi
			log_message "INFO"  "cleanVirtualNetwork - KVM CRC Domain has been stopped."
		fi
        fi
	CRC_Domain_Exists=$(sudo virsh list --all | grep -q crc | awk '{print $2}')
	if [[ 	"${CRC_Domain_Exists}" == "crc" ]]; then 
		sudo virsh undefine crc --remove-all-storage
       		sudo virsh net-destroy crc
	fi

	log_message "INFO"  "cleanVirtualNetwork - KVM CRC Domain has been removed."
        sudo rm -f /etc/NetworkManager/conf.d/crc-nm-dnsmasq.conf /etc/NetworkManager/dnsmasq.d/crc.conf
 	sudo systemctl reload NetworkManager
	log_message "INFO"  "cleanVirtualNetwork - NetworkManager was reloaded."
}

### ------------------------------------- ###
###      connectToCrc
### ------------------------------------- ###
function connectToCrc() {
   	log_message "INFO" "connectToCrc - CRC 'eval $(crc oc-env)' to set oc environmet .."
   	eval $(crc oc-env)
    	# Option 1 - CLUSTER_ADMIN_PASSWD=$(grep -i "To login as an admin, run 'oc login -u kubeadmin -p" /tmp/install_crc.log | awk '{print $13}')
	log_message "INFO" "connectToCrc - Extract credentials"
	#Option2 to get credentials
 	crc console --credentials > ./crc_creds
  	sed -i '1d' ./crc_creds
  	ADMIN_PASS=`cat ./crc_creds | sed -e 's/.*kubeadmin -p \(.*\)https.*/\1/'`
	log_message "INFO" "connectToCrc - login as kubeadmin"
	oc login -u kubeadmin -p $ADMIN_PASS https://api.crc.testing:6443 --insecure-skip-tls-verify=true

	#can't deploy crd:  oc login -u developer -p developer https://api.crc.testing:6443 --insecure-skip-tls-verify=true


	log_message "INFO" "connectToCrc - expose internal registry to load docker images"	
	oc patch configs.imageregistry.operator.openshift.io/cluster --patch '{"spec":{"defaultRoute":true}}' --type=merge

	log_message "INFO" "connectToCrc - Completed"
	# craete a project if not exists already

	oc new-project ${K8S_NAMESPACE}
	oc project ${K8S_NAMESPACE}

}


### ------------------------------------- ###
###      XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
### ------------------------------------- ###

### ------------------------------------- ###
###      UNUSED
### ------------------------------------- ###
function choose_host_address() {

    local CHOOSE_HOST_ADDRESS_RC=0
    local ADDRESS_LIST=""
    local DOCKER_SUBNET=0
    local FIXED_ADDRESS_LIST=""
    local USER_RESPONSE=""
    local USER_RESP_VALID=0
    local IP_ADDRESS_NO=0

    if [[ "mac" = "${OS}" ]]; then
        ADDRESS_LIST=`ifconfig | grep inet | grep -v inet6 | grep -v 127.0.0.1 | awk '{print $2}'`
    else
        ADDRESS_LIST=`hostname -I`
    fi

    log_message "INFO" "choose_host_address - Building host address list"
    for HOST_ADDRESS_FROM_LIST in ${ADDRESS_LIST}
    do
        if [[ ! "mac" = "${OS}" ]]; then
            DOCKER_SUBNET=`ip addr | grep ${HOST_ADDRESS_FROM_LIST} | grep 'docker0\|virbr0\|crc' | wc -l`
        else
            DOCKER_SUBNET=0
        fi
        if [[ ${DOCKER_SUBNET} -eq 0 ]]; then
            if [[ ${IP_ADDRESS_NO} -eq 0 ]]; then
                FIXED_ADDRESS_LIST="${HOST_ADDRESS_FROM_LIST}"
            else
                FIXED_ADDRESS_LIST="${FIXED_ADDRESS_LIST} ${HOST_ADDRESS_FROM_LIST}"
            fi
            IP_ADDRESS_NO=$(( IP_ADDRESS_NO + 1 ))
        else
            log_message "INFO" "choose_host_address -Skipping address ${HOST_ADDRESS_FROM_LIST}. It is a crc/docker/VM internal network."
        fi
    done

    if [[ ${IP_ADDRESS_NO} -gt 1 ]]; then
        log_message "INFO" "choose_host_address -waiting for user to choose valid host address from following list : ${FIXED_ADDRESS_LIST}"

        echo ""
        echo -e "\tPlease select the IP address"
        echo ""

        echo -e "\tThe current configured IPs on this server are : "
        echo ""

        for CURR_IP in "${FIXED_ADDRESS_LIST[@]}" ; do
            echo -e "\t   ${CURR_IP}"
        done

        while [[ ( x"$USER_RESPONSE" != "xy" &&  x"$USER_RESPONSE" != "xY" &&  x"$USER_RESPONSE" != "xyes" &&  x"$USER_RESPONSE" != "xYES" ) ]]; do
            echo
            echo

            USER_RESP_VALID=0
            while [ "$USER_RESP_VALID" = 0 ] ; do
                 echo
                 echo -ne  "\tPlease enter UI console IP address  :> "; read SERVER_IP

                 # make sure server ip is not empty
                if [[ ! -z ${SERVER_IP} ]]; then
                    # make sure the ip address is one for the ip address that are configured on the server
                    if [[ " ${FIXED_ADDRESS_LIST[@]} " =~ " ${SERVER_IP} " ]]; then
                        USER_RESP_VALID=1;
                     else
                        echo
                        echo -e  "\tIP address is not one of the configured IPs on this server."
                        echo -e  "\tPlease enter configured IP address."
                        USER_RESP_VALID=0;

                        echo
                        echo -e "\tThe current configured IPs on this server are : "
                        echo ""

                        for CURR_IP in "${FIXED_ADDRESS_LIST[@]}" ; do
                            echo -e "\t   ${CURR_IP}"
                        done
                     fi
                else
                    echo -e  "\tPlease enter IP address !\n"
                    USER_RESP_VALID=0;
                fi
            done

            echo ""
            echo -e "\tYou entered:"
            echo -e "\tUI console ip address        : ${SERVER_IP}"
            echo ""

            USER_RESPONSE=""
            while [ "$USER_RESPONSE" != "y" ] && [ "$USER_RESPONSE" != "Y" ] && [ "$USER_RESPONSE" != "YES" ] && [ "$USER_RESPONSE" != "yes" ] && [ "$USER_RESPONSE" != "n" ] && [ "$USER_RESPONSE" != "N" ] && [ "$USER_RESPONSE" != "NO" ] && [ "$USER_RESPONSE" != "no" ] ; do
                echo -ne "\tIs this correct? [y/n] "; read USER_RESPONSE
            done


        done

        log_message "INFO" "choose_host_address -User chose ${SERVER_IP} for UI console ip address"
    else
        # there is only one ip address configured on the server -> use it
        SERVER_IP=${FIXED_ADDRESS_LIST[0]}
        log_message "INFO" "choose_host_address -One ip address configured on server, ${SERVER_IP} , using it"
    fi


    CHOSEN_HOST_ADDRESS=${SERVER_IP}

    return ${CHOOSE_HOST_ADDRESS_RC}
}


OS="centos"
LOG_FILE="chooseIP.log"



function old_install_crc() {
    log_message "INFO" "CRC Installation.."
    log_message "INFO" "Check if crc is up.."
    local IS_CRC_UP=0
    crc status >/dev/null 2>&1 && IS_CRC_UP=1
    if [[ ${IS_CRC_UP} -eq 0 ]]; then 



        local IS_CRC_UP=0
        log_message "INFO" "Running CRC start.. - will take about 20 min !!!!"
        crc start -p /tmp/pull-secret.txt ${CRC_DEBUG} > /tmp/install_crc.log 2>&1 && IS_CRC_UP=1
        # crc start -p /tmp/pull-secret.txt --log-level debug >/dev/null 2>&1 && IS_CRC_UP=1  
        if [[ ${IS_CRC_UP} -eq 0 ]]; then 
            IS_OpenShift_Running=$(crc status --log-level debug | grep 'OpenShift' | awk '{print $2}')
            log_message "INFO" "Stopping CRC in order. Check if it is running"
            pause
        #    # this is  an hack based on this: http://www.mastertheboss.com/soa-cloud/openshift/getting-started-with-code-ready-containers
            crc stop 
            crc delete -f
            sudo virsh undefine crc --remove-all-storage
            sudo virsh net-destroy crc
            sudo rm -f /etc/NetworkManager/conf.d/crc-nm-dnsmasq.conf /etc/NetworkManager/dnsmasq.d/crc.conf
            sudo systemctl reload NetworkManager
            crc setup
            crc start -p pull-secret.txt ${CRC_DEBUG} > /tmp/install_crc.log 2>&1
        fi

        log_message "INFO" "Runnig CRC status to validate installation.."
        local IS_CRC_UP=5
        IS_OpenShift_Running=$(crc status --log-level debug | grep 'OpenShift' | awk '{print $2}' && IS_CRC_UP=6 )
        log_message "INFO" "CRC status is ${IS_OpenShift_Running}. If status ok value should be 6 - value:$IS_CRC_UP"

        #TODO handle errors in case IS_CRC_UP=5
        index=1	
        while [[ $index -le 5 && $IS_OpenShift_Running = "Running" ]]; do
            sleep 20s
            IS_OpenShift_Running=$(crc status --log-level debug | grep 'OpenShift' | awk '{print $2}')
            log_message "INFO" "Waiting for openShift to start- wait number:$index"
            (( index++ ))
        done 
        log_message "INFO" "Runnig CRC oc-env to extract oc.."
        # crc oc-env
        log_message "INFO" "Runnig CRC 'eval $(crc oc-env)' to set oc environmet .."
    fi

    eval $(crc oc-env)
    crc console --credentials > ./crc_creds
    sed -i '1d' ./crc_creds
    ADMIN_PASS=`cat ./crc_creds | sed -e 's/.*kubeadmin -p \(.*\)https.*/\1/'`
    oc login -u kubeadmin -p $ADMIN_PASS https://api.crc.testing:6443 --insecure-skip-tls-verify=true
    #can't deploy crd:  oc login -u developer -p developer https://api.crc.testing:6443 --insecure-skip-tls-verify=true
		
    oc patch configs.imageregistry.operator.openshift.io/cluster --patch '{"spec":{"defaultRoute":true}}' --type=merge
}

function reinstall_crc1() {
    log_message "INFO" "CRC Installation.."
    log_message "INFO" "enable libvrt.."
#    sudo yum -y install qemu-kvm libvirt libvirt-python libguestfs-tools virt-install NetworkManager bridge-utils libvirt-daemon-driver-qemu virt-manager

sudo rpm -qa | grep -qw qemu-kvm || yum install qemu-kvm
sudo rpm -qa | grep -qw libvirt || yum install libvirt
sudo rpm -qa | grep -qw libvirt-python || yum install libvirt-python
sudo rpm -qa | grep -qw libguestfs-tools || yum install libguestfs-tools
sudo rpm -qa | grep -qw virt-install || yum install virt-install
sudo rpm -qa | grep -qw NetworkManager || yum install NetworkManager
sudo rpm -qa | grep -qw bridge-utils || yum install bridge-utils
sudo rpm -qa | grep -qw libvirt-daemon-driver-qemu || yum install libvirt-daemon-driver-qemu
sudo rpm -qa | grep -qw virt-manager || yum install virt-manager

    #sudo yum -y install qemu-kvm libvirt virt-install bridge-utils NetworkManager libvirt-daemon-driver-qemu 
    sudo systemctl enable --now libvirtd 
    sudo systemctl restart libvirtd

	log_message "INFO" " !!! IF this line is failint than you should  read the doc - especially this line - For vmware workstation you should make sure to have on the VM machine settings in the 'Processors' section you should have few checkboxes for 'Virtualization engine' and make sure the 'Virtualize Intel VT-x/EPT' checkbox is enabled and all others are disabled."
    sleep 3
    lsmod | grep -i kvm
    sleep 3
    brctl show
	log_message "INFO" "Perform cleanup.."
	rm -fR ~/.crc/
	rm -fR ~/.kube/
    rm -fR /tmp/crc*/
	sudo rm -Rf /usr/local/bin/crc
	sudo rm -f /etc/NetworkManager/conf.d/crc-nm-dnsmasq.conf
	sudo rm -f /etc/NetworkManager/dnsmasq.d/crc.conf
	sudo rm -f /etc/libvirt/qemu/networks/autostart/crc.xml
	sudo rm -f /etc/libvirt/qemu/networks/crc.xml
	sudo rm -f /etc/libvirt/qemu/crc.xml
	sudo rm -f /var/lib/libvirt/dnsmasq/crc.conf
	sudo rm -f /var/lib/libvirt/dnsmasq/crc.hostsfile
	sudo rm -f /var/lib/libvirt/dnsmasq/crc.addnhosts
	sudo rm -f /var/lib/libvirt/dnsmasq/crc.status
	sudo rm -f /var/lib/libvirt/dnsmasq/crc.macs
	sudo rm -fR /var/lib/libvirt/qemu/channel/target/domain-1-crc
	sudo rm -fR /var/lib/libvirt/qemu/domain-1-crc
	sudo rm -f /var/log/libvirt/qemu/crc.log
	#for debug - pause

    log_message "INFO" "Download 2G of CRC binary.."
	curr_dir="$(pwd)"

    log_message "INFO" "COPY CRC to PATH and delete the extracted library.. $curr_dir"
    #For debug cache this file abd set comment on this line UNDO wget $CRC_URL --output-file /tmp/crc-linux-amd64.tar.xz
	cd  /tmp
	if [ ! -f /tmp/crc-linux-amd64.tar.xz ]; then	
		    wget --limit-rate=60k $CRC_URL -o /tmp/crc-linux-amd64.tar.xz -c https://mirror.openshift.com/pub/openshift-v4/clients/crc/1.7.0/crc-linux-amd64.tar.xz
	fi

    log_message "INFO" "Extract CRC binary and delete it.."
    tar xvf /tmp/crc-linux-amd64.tar.xz
    ##TODO - DEV _ ONLY rm -rf /tmp/crc-linux-amd64.tar.xz
	#rm -rf /tmp/crc-linux-amd64.tar.xz


    cd /tmp/crc*/
    sudo cp crc /usr/local/bin
    cd ..
    rm -Rf /tmp/crc*/
	#for debug - pause

    log_message "INFO" "Runnig CRC version.."
    crc version

	# run command "sudo virsh list --all " and if there is an crc domain in kvm you must remove it with command  "sudo virsh undefine crc" (SUDO IS MUST !!)
    log_message "If CRC exists than remove it. Also checking if KVM CRC domain exists and undefine it"
	IS_OpenShift_Running="No CRC installed"
	if [ -f /usr/local/bin/crc ]; then
		IS_OpenShift_Running=$(crc status --log-level debug | grep 'OpenShift' | awk '{print $2}')
		if [[ "${IS_OpenShift_Running}" = "running" ]]; then
		    log_message "Stopping CRC."
					crc stop 
		    log_message "Deleting CRC."
			        crc delete -f
		fi
	fi
	CRC_Domain_Exists=$(sudo virsh list --all | grep crc | awk '{print $2}')
	CRC_Domain_Status=$(sudo virsh list --all | grep crc | awk '{print $3}')
	echo "KVM Domain Exist : --CRC_Domain_Exists:${CRC_Domain_Exists}--CRC Domain Status:${CRC_Domain_Status}---IS_OpenShift_Running:${IS_OpenShift_Running}"
	if [[ "${CRC_Domain_Exists}" = "crc" ]]; then
	    log_message "clean existing CRC domain"
	    if [[ "${CRC_Domain_Status}" = "running" ]]; then
			sudo virsh shutdown crc
			sleep 10s
			CRC_Domain_Status=$(sudo virsh list --all | grep crc | awk '{print $3}')
			if [[ "${CRC_Domain_Status}" != "shut" ]]; then
				#sudo virsh destroy crc
				index1=1	
				while [[ $index1 -le 20 && "${CRC_Domain_Status}" != "shut" ]]; do
					sleep 10s
				    CRC_Domain_Status=$(sudo virsh list --all | grep crc | awk '{print $3}')
					log_message "INFO" "CRC Domain is still ${CRC_Domain_Status} - wait number:$index1 for another 10s"
					(( index1++ ))
				done 
			fi
			CRC_Domain_Status=$(sudo virsh list --all | grep crc | awk '{print $3}')
		fi
        sudo virsh undefine crc --remove-all-storage
        sudo virsh net-destroy crc

        sudo rm -f /etc/NetworkManager/conf.d/crc-nm-dnsmasq.conf /etc/NetworkManager/dnsmasq.d/crc.conf
		rm -fR ~/.crc/
		rm -fR ~/.kube/
		sudo rm -f /etc/libvirt/qemu/networks/autostart/crc.xml
		sudo rm -f /etc/libvirt/qemu/networks/crc.xml
		sudo rm -f /etc/libvirt/qemu/crc.xml
		sudo rm -f /var/lib/libvirt/dnsmasq/crc.conf
		sudo rm -f /var/lib/libvirt/dnsmasq/crc.hostsfile
		sudo rm -f /var/lib/libvirt/dnsmasq/crc.addnhosts
		sudo rm -f /var/lib/libvirt/dnsmasq/crc.status
		sudo rm -f /var/lib/libvirt/dnsmasq/crc.macs
		sudo rm -fR /var/lib/libvirt/qemu/channel/target/domain-1-crc
		sudo rm -fR /var/lib/libvirt/qemu/domain-1-crc
		sudo rm -f /var/log/libvirt/qemu/crc.log

        sudo systemctl reload NetworkManager

	fi
    log_message "INFO" "Completed CRC Cleanup"
	#for debug - pause

    log_message "INFO" "Running CRC setup.."
    crc setup

    ### ovveride dns issues https://medium.com/@trlogic/how-to-setup-local-openshift-4-cluster-with-red-hat-codeready-containers-6c5aefba72ad
	# extract IP that was set in "crc setup" in /etc/NetworkManager/dnsmasq.d/crc.conf
	my_ip=$(cat /etc/NetworkManager/dnsmasq.d/crc.conf | grep 'server=/crc.testing/' | awk -F '/' '{print $3}')
	log_message "INFO" "Extract CRC IP after setup: $my_ip"
	my_host="api.crc.testing"
	sudo sed -i "/$my_host/ s/.*/$my_ip\t$my_host/g" /etc/hosts

	my_host="console-openshift-console.apps-crc.testing"
	sudo sed -i "/$my_host/ s/.*/$my_ip\t$my_host/g" /etc/hosts

	my_host="oauth-openshift.apps-crc.testing"
	sudo sed -i "/$my_host/ s/.*/$my_ip\t$my_host/g" /etc/hosts
	
	sudo sed -i -n -e '/^search localdomain/!p' -e '$asearch redhat.com' /etc/resolv.conf

    log_message "INFO" "read redhat image pull secret if exists .."
	if [ ! -f /tmp/pull-secret.txt ]; then
		read -p "Please paste redhat image pull secret (https://cloud.redhat.com/openshift/install/crc/installer-provisioned) and press [ENTER]:"
		echo "$REPLY" > /tmp/pull-secret.txt
	fi

    local IS_CRC_UP=0
    

    log_message "INFO" "Running CRC start.. - will take at least 20 min !!!!"
    crc start -p /tmp/pull-secret.txt ${CRC_DEBUG} > /tmp/install_crc.log 2>&1 && IS_CRC_UP=1
    # crc start -p /tmp/pull-secret.txt --log-level debug >/dev/null 2>&1 && IS_CRC_UP=1  

    # in case of a failure we will need to reinstall with a woekaround
    if [[ ${IS_CRC_UP} -ne 0 ]]; then 
	Is_Memory_Problem=$(grep -i '(crc) Failed to start:.*Cannot allocate memory.*'   /tmp/install_crc.log)
	if [[ -n ${Is_Memory_Problem} ]]; then
       		log_message "ERROR" "Stopping CRC - memory allocation issue detected. Check Operating system prereq"		
	fi
	IS_OpenShift_Running=$(crc status --log-level debug | grep 'OpenShift' | awk '{print $2}')
       	log_message "INFO" "Stopping CRC - failure detected. Check if it is running"


    #    # this is  an hack based on this: http://www.mastertheboss.com/soa-cloud/openshift/getting-started-with-code-ready-containers
        crc stop 
        crc delete -f
		#TODO check if domain CRC exist before run the virsh command
        sudo virsh undefine crc --remove-all-storage
        sudo virsh net-destroy crc
        sudo rm -f /etc/NetworkManager/conf.d/crc-nm-dnsmasq.conf /etc/NetworkManager/dnsmasq.d/crc.conf
        sudo systemctl reload NetworkManager
        crc setup
        crc start -p /tmp/pull-secret.txt ${CRC_DEBUG} > /tmp/install_crc.log 2>&1
 
	
    fi

    log_message "INFO" "Runnig CRC status to validate installation.."
	local IS_CRC_UP=5
    IS_OpenShift_Running=$(crc status --log-level debug | grep 'OpenShift' | awk '{print $2}')
	crc status && IS_CRC_UP=6
    log_message "INFO" "CRC status is ${IS_OpenShift_Running}. If status ok value should be 6 - value:$IS_CRC_UP"

	#handle errors in case IS_CRC_UP=5
	if [[ ${IS_CRC_UP} -eq 5 ]]; then
		log_message "INFO" "CRC status is ${IS_OpenShift_Running} and interl value is :$IS_CRC_UP which means you should rerun script to init environment"
		exit
	fi

	index=1	
	while [[ $index -le 5 && $IS_OpenShift_Running != "Running" ]]; do
		sleep 20s
	    IS_OpenShift_Running=$(crc status --log-level debug | grep 'OpenShift' | awk '{print $2}')
		log_message "INFO" "Waiting for openShift to start- wait number:$index"
		(( index++ ))
	done 
    log_message "INFO" "Runnig CRC oc-env to extract oc.."
    # crc oc-env
    log_message "INFO" "Runnig CRC 'eval $(crc oc-env)' to set oc environmet .."
    eval $(crc oc-env)
	CLUSTER_ADMIN_PASSWD=$(grep -i "To login as an admin, run 'oc login -u kubeadmin -p" /tmp/install_crc.log | awk '{print $13}')
    #oc login -u developer -p developer https://api.crc.testing:6443 --insecure-skip-tls-verify=true
	oc login -u kubeadmin -p ${CLUSTER_ADMIN_PASSWD} https://api.crc.testing:6443 --insecure-skip-tls-verify=true
	
	# craete a project if not exists already

	oc new-project ${K8S_NAMESPACE}
	oc project ${K8S_NAMESPACE}
}




function add_operator_project() {
    ## install example  https://github.com/operator-framework/operator-sdk/blob/master/doc/user-guide.md
    log_message "INFO" "create projects folder and cd"
    rm -fR $HOME/projects
    mkdir -p $HOME/projects
    cd $HOME/projects
    log_message "INFO" "create project memcached-operator with sdk in ${pwd}"
    /usr/local/bin/operator-sdk new memcached-operator --repo=github.com/example-inc/memcached-operator
    cd memcached-operator

    log_message "INFO" "generate api in ${pwd}"
    /usr/local/bin/operator-sdk add api --api-version=cache.example.com/v1alpha1 --kind=Memcached

    log_message "INFO" "Override types code  in ${pwd}/pkg/apis/cache/v1alpha1/memcached_types.go"
    cp ${GITDIR}/memcached_types.go.tmpl $HOME/projects/memcached-operator/pkg/apis/cache/v1alpha1/memcached_types.go

    log_message "INFO" "regenerate K8S after code change in ${pwd}"
    /usr/local/bin/operator-sdk generate k8s

    log_message "INFO" "generate crds in ${pwd}"
    /usr/local/bin/operator-sdk generate crds

    log_message "INFO" "generate controller in ${pwd}"
    /usr/local/bin/operator-sdk add controller --api-version=cache.example.com/v1alpha1 --kind=Memcached

    log_message "INFO" "Override code of controller in ${pwd}/pkg/controller/memcached/memcached_controller.go"
    cp ${GITDIR}/memcached_controller.go.tmpl $HOME/projects/memcached-operator/pkg/controller/memcached/memcached_controller.go


    log_message "INFO" "run locally withing cluster - deploy crd..."

    $KUBE_CLI create -f deploy/crds/cache.example.com_memcacheds_crd.yaml
    # validation

    log_message "INFO" "Building memcached-operator:v0.0.1 and deploying it to docker daemon"
    #sudo eval $(minikube docker-env)
    #sudo "PATH=$PATH" /usr/local/bin/operator-sdk build memcached-operator:v0.0.1
    /usr/local/bin/operator-sdk build memcached-operator:v0.0.1
        
    if [[ $MINIKUBE_INSTALLATION = "yes" ]]; then
        # ignore currently - deploy images locally
            
        sed -i "s|REPLACE_IMAGE|memcached-operator:v0.0.1|g" deploy/operator.yaml
        sed -i "s|imagePullPolicy: Always|imagePullPolicy: Never|g" deploy/operator.yaml

    else
        #CRC
        #OCP_NEW_PROJECT=operator-test 
        sed -i "s|REPLACE_IMAGE|image-registry.openshift-image-registry.svc:5000/default/memcached-operator:v0.0.1|g" deploy/operator.yaml
            
        OCP_REGISTRY_HOST=$(oc get route default-route -n openshift-image-registry --template='{{ .spec.host }}')
        sudo cat <<EOF | sudo tee -a /etc/docker/daemon.json
{
    "insecure-registries": [
        "default-route-openshift-image-registry.apps-crc.testing"
    ]
}
EOF
        sudo service docker restart
           
        #oc new-project OCP_NEW_PROJECT
        #There's a bug in oc whoami
        sudo setfacl --modify user:admin:rw /var/run/docker.sock
        docker login -u kubeadmin -p $(oc whoami -t)  $OCP_REGISTRY_HOST
        docker tag memcached-operator:v0.0.1 $OCP_REGISTRY_HOST/default/memcached-operator:v0.0.1
        docker push $OCP_REGISTRY_HOST/default/memcached-operator:v0.0.1               
    fi
    

    log_message "INFO" " Deploying Operator:"
    $KUBE_CLI create -f deploy/service_account.yaml
    $KUBE_CLI create -f deploy/role.yaml
    $KUBE_CLI create -f deploy/role_binding.yaml
    $KUBE_CLI create -f deploy/operator.yaml

    log_message "INFO" " operator deployment"
    $KUBE_CLI get deployment


    log_message "INFO" " Deploying CR with 3 pods"
    cat deploy/crds/cache.example.com_v1alpha1_memcached_cr.yaml

    log_message "INFO" " operator and CR deployment and pods"
    $KUBE_CLI get deployment
    $KUBE_CLI get pods -o wide -n ${K8S_NAMESPACE}

    log_message "INFO" " sleep 30 secs to let k8s wake up operator"
    sleep 30s # 30 seconds to let kubectl to get up
    log_message "INFO" " operator and CR deployment and pods"
    $KUBE_CLI get deployment
    $KUBE_CLI get pods -o wide -n ${K8S_NAMESPACE}

    $KUBE_CLI apply -f deploy/crds/cache.example.com_v1alpha1_memcached_cr.yaml

    log_message "INFO" " pods of example-memcached:"
    $KUBE_CLI get pods -o wide -n ${K8S_NAMESPACE} | grep memcached
    NUMBER_OF_PODS=$(($KUBE_CLI get pods -o wide -n ${K8S_NAMESPACE} | grep example-memcached ) | wc -l)

    if [[ $NUMBER_OF_PODS = 3 ]]; then
        log_message "INFO" " pods of example-memcached is 3: Validation Succeeded! "
    else
        log_message "ERROR" " pods of example-memcached is $NUMBER_OF_PODS and not 3: Validation FAILED! "
        exit 1
    fi

    log_message "INFO" " replace size in CR to 5"
    sed -i "s|size: 3|size: 5|g" deploy/crds/cache.example.com_v1alpha1_memcached_cr.yaml
    cat deploy/crds/cache.example.com_v1alpha1_memcached_cr.yaml
    $KUBE_CLI apply -f deploy/crds/cache.example.com_v1alpha1_memcached_cr.yaml

    log_message "INFO" " sleep 5 secs to let k8s wake up pods"
    sleep 5 # 5 seconds to let kubectl to get up
    $KUBE_CLI get pods -o wide -n ${K8S_NAMESPACE} | grep memcached

    NUMBER_OF_PODS=$(($KUBE_CLI get pods -o wide -n ${K8S_NAMESPACE} | grep example-memcached ) | wc -l)

    if [[ $NUMBER_OF_PODS = 5 ]]; then
        log_message "INFO" " pods of example-memcached is 5: Validation Succeeded! "
    else
        log_message "ERROR" " pods of example-memcached is $NUMBER_OF_PODS and not 5: Validation FAILED! "
        exit 1
    fi

    log_message "INFO" " Cleanind up CR and operator"
    $KUBE_CLI delete -f deploy/crds/cache.example.com_v1alpha1_memcached_cr.yaml
    $KUBE_CLI delete -f deploy/operator.yaml
    $KUBE_CLI delete -f deploy/role_binding.yaml
    $KUBE_CLI delete -f deploy/role.yaml
    $KUBE_CLI delete -f deploy/service_account.yaml

    log_message "INFO" "run locally withing cluster - delete crds..."
    $KUBE_CLI delete -f deploy/crds/cache.example.com_memcacheds_crd.yaml

    log_message "INFO" "add VSCode project"
    code $HOME/projects/memcached-operator --user-data-dir /home/${USER}/.config/Code
    log_message "INFO" "add VSCode go extension"
    code --install-extension ms-vscode.Go

}


#################
# Invoking main #
#################
main "$@"
