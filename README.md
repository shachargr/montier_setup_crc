# MonTier K8S Operator Development Environment #

* This repo will hold the scripts and tools to build a golang operator environment for Montier app and cluser operator
This Guide show how to install operaor SDK plus all prerequesites on CENTOS 7.7 machine.
The script is based on the following scripts:
https://github.com/operator-framework/operator-sdk/blob/master/doc/user-guide.md

* This Guide show how to install the prerequistes: Git (we assume it is installed already) , golang , mercurial , docker (remove previous installations to be complient with the version) , and kubectl. The guide also assume an access to K8S cluster - we emulated this cluster by installing Minikube over kvm2 hypervisor.
(based on https://computingforgeeks.com/how-to-install-minikube-on-centos-linux-with-kvm/)

* Please note that the second part of the document shows how to use the operator SDK and create sample of memcached operator.

* **Version:** 1.0
* **Source:** [MonTier K8S Operator Development Environment](https://bitbucket.org/montier/od4ciptracingdemo)
* **License:** [Artistic License 2.0](LICENSE.md)
* **Copyright:** Copyright (C) 2015-2019 by [MonTier Software (2015) LTD](http://www.mon-tier.com/)




# Prerequisites
* For vmware workstation you should make sure to have on the VM machine settings in the  "Processors" section you should have few checkboxes for "Virtualization engine" and make sure the "Virtualize Intel VT-x/EPT" checkbox is enabled and all others are disabled.
* Your VM machine should have 16GB of Memory minimum of 6 vCPUs and 45GB free space just for installing CRC !! We recomend to use 500GB !
* This script expects a CENTOS 7.7 machine 
	* To check your RH CENTOS release run this
```
> cat /etc/redhat-release
CentOS Linux release 7.7.1908 (Core)

```

* You need a user who is sudoer.
	* This is how you can create a user (for example admin) and make it a sudoer (by default wheel group in centOS grants you sudoers permissions):
```
adduser admin
passwd admin
usermod -aG wheel admin
su - admin
sudo ls -la /root
```



* you need to have git 1.8+ installed
	* you can install it  by running
```
yum install git
```


# Downloading and Setting Up the Enviroment
Copy the command below and paste it in your SSH terminal (you may need to enter a password for the sudo command):
```bash
cd ~
sudo rm -fR ~/git/montier_k8s_operator
```
```bash
cd ~
mkdir -p ~/git/montier_k8s_operator
cd  ~/git/montier_k8s_operator
git clone https://hanank@bitbucket.org/montier/od4ciptracingdemo.git .

```
## Install with CRC (Recommended)
enable CRC:
```
./install-operator-sdk.sh 
```

In order to create a pull-secret.txt file (please replace my pull secret).
based on https://cloud.redhat.com/openshift/install/crc/installer-provisioned
- Perform a login 
Go to "Pull secret: 
Download or copy your pull secret. The install program will prompt you for your pull secret during installation."
- copy this file to base root

## Install with MiniKube (Deprecated)
*NOTE*: Currently this script is not enabling by default minikube installation. to run with minikube install:

```
./install-operator-sdk.sh -m 
or
./install-operator-sdk.sh --minikube  
```
NOTE: just after running it it will need the admin user password (for sudo within the script).

By default we set it to run by docker driver and not virtualization.

The driver can be changed in build.properties file to other heavyweight driver:
```
MINIKUBE_DRIVER=virtualbox
or
MINIKUBE_DRIVER=kvm2
```

Please enable those drivers if Your CENTOS enables virtualization.(kvm2 virtualization is based on libvirt service)





### Is there another way to set up? ###

* remote copy to cenr
in order to run the script remote copy the following files:
build.properties - a file that holds the external URLs of the script installations.
install-operator-sdk.sh - the script itself
*.tmpl files - code replacement in generated code

```
scp build.properties admin@YOUR_SERVER:/home/admin/
scp install-operator-sdk.sh admin@YOUR_SERVER:/home/admin/
scp /*.tmpl admin@YOUR_SERVER:/home/admin/

```

In validation phase there is an option within the properties file to rebuild the operator image to docker deamon rather then pulling it from docker hub (option that is not valid anymore).

in build.properties therefor is set to:

```
SKIP_BUILD=no
```
Don't change it.


ssh to your machine as admin and run without sudo:
```
chmod +x install-operator-sdk.sh
./install-operator-sdk.sh -m
```
-m stands for minikube (based by default on docker)

* Configuration - in build.properties file
```
RELEASE_VERSION=v0.15.2
OPERATOR_SDK_LINK=https://github.com/operator-framework/operator-sdk/releases/download/${RELEASE_VERSION}/operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu
OPERATOR_SDK_LINK_ASC=https://github.com/operator-framework/operator-sdk/releases/download/${RELEASE_VERSION}/operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu.asc
#INSTALL_EPEL_BAZZAR=http://dl.fedoraproject.org/pub/epel/5/i386/epel-release-5-4.noarch.rpm
INSTALL_MERCURIAL=https://www.mercurial-scm.org/release/centos7/mercurial-5.3.1-1.x86_64.rpm
MERCURIAL_RPM=mercurial-5.3.1-1.x86_64.rpm
DOCKER_REPO=https://download.docker.com/linux/centos/docker-ce.repo
KUBECTL_REPO=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
KUBECTL_REPO_GPG='https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg'
MINIKUBE_URL=https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
GO_VERSION=1.13.8 
GOLANG_URL=https://dl.google.com/go/go$GO_VERSION.linux-amd64.tar.gz
```

* DoD
successfull script running & new go operator project located in ~/projects/memcached-operator
or rerun the last test of running operator locally.

Set the name of the operator in an environment variable:

```
export OPERATOR_NAME=memcached-operator
```

Run the operator locally with the default Kubernetes config file present at $HOME/.kube/config:
```
$ operator-sdk run --local --watch-namespace=default

2018/09/30 23:10:11 Go Version: go1.10.2
2018/09/30 23:10:11 Go OS/Arch: darwin/amd64
2018/09/30 23:10:11 operator-sdk Version: 0.0.6+git
2018/09/30 23:10:12 Registering Components.
2018/09/30 23:10:12 Starting the Cmd.
```


### Who do I talk to? ###

* Repo owner or admin
